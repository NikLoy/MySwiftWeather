//
//  WeatherViewController.swift
//  MyWeather
//
//  Created by Nicolas Lourenco on 08/06/16.
//  Copyright © 2016 Nicolas Lourenco. All rights reserved.
//

import UIKit
import Foundation

class WeatherViewController: UIViewController {//, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableViewView: UIView!
    
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var skyStateLabel:UILabel!
    @IBOutlet var temperature: UILabel!
    
    @IBOutlet var sunriseLabel: UILabel!
    @IBOutlet var sunsetLabel: UILabel!
    @IBOutlet var cloudsLabel: UILabel!
    @IBOutlet var rainLabel: UILabel!
    @IBOutlet var humiditylabel: UILabel!
    @IBOutlet var pressionLabel:UILabel!
    
    private var city: String!
    private var cityInfos: Dictionary<String,AnyObject>?
    private var weekWeather: Array<AnyObject>?
    
    private var currentWeather: Dictionary<String,AnyObject>?
    private var currentTemp: Float?
    private var currentSky: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Weather"
        self.city = "Lyon"
        
        self.updateWeather()
    }
    
    private func updateWeather() {
        
        SimpleRequestManager.sharedService.getCurrentDay(self.city, onSuccess: { (data) -> Void? in
            var responseDict: Dictionary<String,AnyObject>?
            
            do {
                try responseDict = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? Dictionary<String, AnyObject>
            } catch {
                dispatch_sync(dispatch_get_main_queue(), {
                    self.showAlert("Weather for today can't be load. Try again.")
                })
            }
            
            if responseDict != nil {
                self.currentWeather = responseDict
                
                if ((responseDict!["weather"]![0]!["description"] != nil)
                    && (responseDict!["main"]!["temp"] != nil)) {
                    self.currentSky = responseDict!["weather"]![0]!["description"] as? String
                    self.currentTemp = responseDict!["main"]!["temp"] as? Float
                }
                
                dispatch_sync(dispatch_get_main_queue(), {
                    self.reloadData()
                })
                
            }
            return nil
        }) { (error) -> Void? in
            dispatch_sync(dispatch_get_main_queue(), {
                self.showAlert("Weather for today can't be load. Try again.")
            })
            return nil
        }
        
        SimpleRequestManager.sharedService.getFuthersDays(self.city, onSuccess: { (data) -> Void? in
            var responseDict: Dictionary<String,AnyObject>?
            
            do {
                try responseDict = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? Dictionary<String, AnyObject>
            } catch {
                dispatch_sync(dispatch_get_main_queue(), {
                    self.showAlert("Weather for further days can't be load. Try again.")
                })
            }
            
            if responseDict != nil {
                if let city = responseDict!["city"] {
                    self.cityInfos = city as? Dictionary<String, AnyObject>
                }
                
                if let list = responseDict!["list"] {
                    self.weekWeather = list as? Array<AnyObject>
                }
                
                dispatch_sync(dispatch_get_main_queue(), {
                    self.reloadData()
                })
            }
            
            return nil
        }) { (error) -> Void? in
            dispatch_sync(dispatch_get_main_queue(), {
                self.showAlert("Weather for today can't be load. Try again.")
            })
            
            return nil
        }
    }

    private func reloadData() {
        
        // Reload table data
        self.tableView.reloadData()
        
        //Refresh other labels
        self.cityLabel.text = self.city
        self.skyStateLabel.text = self.currentSky
        
        if self.currentTemp != nil {
            self.temperature.text = String(format: "%.f°", self.currentTemp! / 10)
        }
        
        if let sunrise = self.currentWeather?["sys"]?["sunrise"] as? Double {
            let sunriseDate = NSDate(timeIntervalSince1970: sunrise)
            self.sunriseLabel.text = self.stringWithDateFormat("hh:mm aa", fromDate: sunriseDate )
        }
        
        if let sunset = self.currentWeather!["sys"]!["sunset"]! as? Double {
            let sunsetDate = NSDate(timeIntervalSince1970: sunset)
            self.sunsetLabel.text = self.stringWithDateFormat("hh:mm aa", fromDate: sunsetDate )
        }
        
        if (self.currentWeather!["clouds"]!["all"] as? Float != nil) {
            let cloud = self.currentWeather!["clouds"]!["all"] as? Float
            self.cloudsLabel.text = String(format: "%.2f %%", cloud!)
        }

        if (self.currentWeather!["rain"]!["3h"] as? Float != nil) {
            let rain = self.currentWeather!["rain"]!["3h"] as? Float
            self.rainLabel.text = String(format: "%.2f mm", rain!)
        }

        if (self.currentWeather!["main"]!["humidity"] as? Float != nil) {
            let humidity = self.currentWeather!["main"]!["humidity"] as? Float
            self.humiditylabel.text = String(format: "%.2f %%", humidity!)
        }
        
        if (self.currentWeather!["main"]!["pressure"] as? Float != nil) {
            let pressure = self.currentWeather!["main"]!["pressure"] as? Float
            self.pressionLabel.text = String(format: "%.f hPa", pressure!)
        }
    }
    
    //Mark - Table View Data Source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }


    //Mark - Table View Delegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let reusableCellName: String = "WeekWeatherTableViewCell"
        
        var cell: WeekWeatherTableViewCell? = tableView.dequeueReusableCellWithIdentifier(reusableCellName) as? WeekWeatherTableViewCell
        
        if cell == nil {
            cell = WeekWeatherTableViewCell(style: .Default, reuseIdentifier: reusableCellName)
        }
        
        if self.weekWeather?[indexPath.row]["dt"] != nil {
            cell!.dayLabel.text = self.dayFromTimestamp(self.weekWeather?[indexPath.row]["dt"]! as! Double)
        }
        
        if var min: Float = self.weekWeather?[indexPath.row]["temp"]!!["min"] as? Float {
            min = min / 10
            cell!.minimumValue.text = NSString(format: "%.f°", min) as String
        }
        
        if var max: Float = self.weekWeather?[indexPath.row]["temp"]!!["max"] as? Float {
            max = max / 10
            cell!.maximumValue.text = NSString(format: "%.f°", max) as String
        }
        
        return cell!
        
    }
    
    //Mark - Utils
    private func showAlert(message: String) -> Void {
        
        let alert: UIAlertController = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        let ok: UIAlertAction = UIAlertAction(title: "ok", style: .Default) { (action) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }
        
        alert.addAction(ok)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    private func dayFromTimestamp(timestamp: NSTimeInterval) -> String {
        
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        
        return dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: timestamp))
    }
    
    private func stringWithDateFormat(dateFormat: String, fromDate: NSDate) -> String {
            
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = dateFormat
        
        return formatter.stringFromDate(fromDate)
    }

}

//
//  HTTPRequest.swift
//  MyWeather
//
//  Created by Nicolas Lourenco on 08/06/16.
//  Copyright © 2016 Nicolas Lourenco. All rights reserved.
//

import Foundation

typealias HTTPFailBlock = (NSError) -> Void?
typealias HTTPSuccessBlock = (NSData) -> Void?

@objc class MyHTTPRequest: NSObject {
    //typedef void (^HTTPFailBlock)(NSError *error);
    var success: HTTPSuccessBlock!
    var fail: HTTPFailBlock!
    var data: NSMutableData
    
    init(success: HTTPSuccessBlock, fail: HTTPFailBlock) {
        self.data = NSMutableData(capacity: 200)!
        self.success = {(data: NSData) -> Void in
                success(data)
        }

        self.fail = {(error: NSError) -> Void in
                fail(error)
        }
    }
    
    //Mark - Publics methods
    class func sendRequest(request: NSURLRequest, sessionConfig: NSURLSessionConfiguration?, onSuccess: HTTPSuccessBlock, onFailure: HTTPFailBlock) {
        MyHTTPRequest(success: onSuccess, fail: onFailure) .sendRequest(request, sessionConfig: sessionConfig)
    }
    
    private func sendRequest(request: NSURLRequest, sessionConfig: NSURLSessionConfiguration?) {
        
        var session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        if sessionConfig != nil {
            session = NSURLSession(configuration: sessionConfig!)
        }
        
        let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) in
            if ((error == nil))
            {
                let httpResp: NSHTTPURLResponse = response as! NSHTTPURLResponse
                
                if data?.length > 0 && httpResp.statusCode < 399
                {
                    self.success (data!)
                }
                else
                {
                    self.fail (NSError(domain: "com.myWeather.network", code: httpResp.statusCode,
                        userInfo:["data": data!]))
                }
            }
            else
            {
                self.fail (error!);
            }
        }
        
        task.resume()
    }
    
    }

//
//  SimpleRequestManager.swift
//  MyWeather
//
//  Created by Nicolas Lourenco on 08/06/16.
//  Copyright © 2016 Nicolas Lourenco. All rights reserved.
//

import Foundation

class SimpleRequestManager: NSObject {
    let APIKey: String = "5908560b0d8ea7c7222ce7cd645b70c9"
    
    //Mark - Private methods
    private func initObject() -> SimpleRequestManager {
        //Init here
        return self
    }
    
    private func request(route: String) -> NSURLRequest {
        
        let url: NSURL = NSURL(string: "http://api.openweathermap.org/data/2.5/\(route)&APPID=\(APIKey)")!
        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
    
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        
        return request;
    }
    
    //Mark - Publics methods
    static let sharedService = SimpleRequestManager().initObject()
   
    func getCurrentDay(city: String?, onSuccess: HTTPSuccessBlock, onFailure: HTTPFailBlock) {

        /* weather?lat=35&lon=139 */
        var route: String!
    
        if city != nil {
            route = "weather?q=\(city!)"
        } else {
            //Location manager
            //        NSString stringWithFormat:@"weather?lat=%@&lon=%@",
        }
    
        let request: NSURLRequest = self.request(route)
        
        MyHTTPRequest.sendRequest(request, sessionConfig: nil, onSuccess: { (data) -> Void? in
            onSuccess(data)
        })
        { (error) -> Void? in
            onFailure(error)
        }
    }
    
    func getFuthersDays(city: String?, onSuccess: HTTPSuccessBlock, onFailure: HTTPFailBlock) -> Void {
        
        /* forecast/daily?lat=35&lon=139&cnt=8 */
        var route: String!
        
        if city != nil {
            route = "forecast/daily?q=\(city!)&cnt=8"
        } else {
            //Location manager
            //        NSString stringWithFormat:@"weather?lat=%@&lon=%@",
        }
    
        let request: NSURLRequest = self.request(route)
        
        MyHTTPRequest.sendRequest(request, sessionConfig: nil, onSuccess: { (data) -> Void? in
            onSuccess(data)
            })
        { (error) -> Void? in
            onFailure(error)
        }
    }
}
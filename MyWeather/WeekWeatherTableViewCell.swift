//
//  WeekWeatherTableViewCell.swift
//  MyWeather
//
//  Created by Nicolas Lourenco on 09/06/16.
//  Copyright © 2016 Nicolas Lourenco. All rights reserved.
//

import Foundation
import UIKit

class WeekWeatherTableViewCell: UITableViewCell {
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var minimumValue: UILabel!
    @IBOutlet var maximumValue: UILabel!
}